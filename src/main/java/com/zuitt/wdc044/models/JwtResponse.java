package com.zuitt.wdc044.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static  final long serialVersionUID = 1790123481235198342L;
    public final String jwtToken;
    public  JwtResponse(String jwtToken){
        this.jwtToken = jwtToken;
    }

    public  String getJwtToken(){
        return this.jwtToken;
    }
}
